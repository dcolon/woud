# Woud
## Introduction
A forest simulation written in C++ using SDL2 for graphics.
Created to play with SDL and cellular automata.

## Dependencies
* libSDL2
* libSDL2_ttf
* Doxygen for docs

## Building
Simply run `make all`.
Run `make docs`.

## Running
Navigate to the build directory, make sure 'woud' is executable and run it.

