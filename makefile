# Makefile

# Requires:
# SDL2, SDL2_ttf

# Executable info
executable             := woud

# Compiler/linker settings
compiler               := g++
compiler_c             := gcc
compiler_flags         := -c -Wall -Ofast -std=c++0x
compiler_c_flags       := -c -Wall -O2
compiler_flags_debug   := -c -g -Wall -std=c++0x
compiler_c_flags_debug := -c -g -Wall
libraries              := 
linker_flags           := -lSDL2 -lSDL2_ttf

# Source and build dir
src_dir                := src
dst_dir                := build
prf_dir                := profiles
doc_dir                := docs

# Source directories
src_dir_utils          := $(src_dir)/utils
src_dir_lib            := $(src_dir)/lib
src_dir_base           := $(src_dir)/base
src_dir_objects        := $(src_dir)/objects
src_dir_legacy         := $(src_dir)/legacy

# Asset folders to be copied
src_dir_assets         := $(src_dir)/assets

# Source files
src_files_utils        := dccolor.cpp
src_c_files_lib        := 
src_files_lib          := sdl/sdlwindow.cpp
src_files_base         := 
src_files_objects      := population.cpp tree.cpp lightning.cpp
src_files_legacy       := 
src_files              := quadtree.cpp woud.cpp main.cpp

# Source paths for different dirs
path_src_files_utils   := $(patsubst %,$(src_dir_utils)/%,$(src_files_utils))
path_src_c_files_lib   := $(patsubst %,$(src_dir_lib)/%,$(src_c_files_lib))
path_src_files_lib     := $(patsubst %,$(src_dir_lib)/%,$(src_files_lib))
path_src_files_base    := $(patsubst %,$(src_dir_base)/%,$(src_files_base))
path_src_files_objects := $(patsubst %,$(src_dir_objects)/%,$(src_files_objects))
path_src_files_legacy  := $(patsubst %,$(src_dir_legacy)/%,$(src_files_legacy))
path_src_files         := $(patsubst %,$(src_dir)/%,$(src_files))

# Combined source paths
path_src_c_files       := $(path_src_c_files_lib)
path_src_cpp_files     := $(path_src_files_utils) $(path_src_files_lib) $(path_src_files_base) $(path_src_files_objects) $(path_src_files_legacy) $(path_src_files)
path_src_all_files     := $(path_src_c_files) $(path_src_cpp_files)

# Tell make where to look for sources
VPATH                  := $(sort $(dir $(path_src_all_files)))

# Destination paths
path_dst_executable    := $(dst_dir)/$(executable)
path_dst_cpp_objects   := $(patsubst %.cpp,$(dst_dir)/%.o,$(notdir $(path_src_cpp_files)))
path_dst_c_objects     := $(patsubst %.c,$(dst_dir)/%.o,$(notdir $(path_src_c_files)))

# Recipes
all: init $(path_src_c_files) $(path_src_cpp_files) $(executable) $(dst_dir)/assets
	@echo Finished building $(executable)

debug: setdebugflags all

d: debug

setdebugflags:
	@echo Setting debug flags
	$(eval compiler_flags=$(compiler_flags_debug))

init:
	@echo Creating build dir
	mkdir -p $(dst_dir)
	
$(executable): $(path_dst_cpp_objects) $(path_dst_c_objects)
	@echo Linking
	$(compiler) $(libraries) $(path_dst_c_objects) $(path_dst_cpp_objects) -o $(path_dst_executable) $(linker_flags)

$(dst_dir)/%.o: %.cpp
	$(compiler) $(compiler_flags) $< -o $@

$(dst_dir)/%.o: %.c
	$(compiler_c) $(compiler_c_flags) $< -o $@

$(dst_dir)/assets:
	@echo Copying assets
	cp -r ${src_dir_assets} ${dst_dir}

clean:
	@echo Cleaning build dir
	rm -rf ${dst_dir}/*

# Profiling
profile: $(prf_dir) $(executable)
	@echo Starting in profiler
	valgrind --tool=callgrind --callgrind-out-file=profiles/callgrind.out.%p $(path_dst_executable)

$(prf_dir):
	@echo Creating profiles directory
	mkdir -p $@

# Doxygen
doc: $(doc_dir)
	@echo Generating documentation using doxygen
	doxygen Doxyfile

cdoc: $(doc_dir)
	@echo Cleaning doc directory
	find $(doc_dir)/ -not -path '*/.*/*' -type f -not -name 'README.md' -not -name '.*' -print0 | xargs -0 rm -r --

$(doc_dir):
	mkdir -p $(doc_dir)