#include "woud.h"

// Private

/// \brief Tracks framerate when called every iteration
///
/// Called every iteration.
/// Increases the framerateBuffer by one.
/// Gets the current time to see if a second has passed since last debuffering the framerate.
/// If so it will write the buffer to framerate and reset the buffer to 0.
void Woud::processFrameRate() {
	// Add the frame
	frameRateBuffer++;
	// Get the current time and compare it to the previous time
	std::time_t ACurrentTime = std::time(0);
	if(ACurrentTime > frameRateLastSampleTime) {
		// Set the new framerate. Don't forget the frame that brought us here
		frameRate               = frameRateBuffer;
		frameRateBuffer         = 0;
		frameRateLastSampleTime = ACurrentTime;
		// Call for a redraw if the status is showing
		if(drawStatus) {
			needRedraw = true;
		}
	}
}

/// \brief Uses the SDLWindow to initialise the textures we will use to render the simulation
void Woud::initialiseTextures() {
	window->generateCircleTexture(&treeTexture,      treeMaxSize / 2);
	window->generateCircleTexture(&bloomTexture,     treeBloomRadius);
	window->generateCircleTexture(&fireTexture,      fireSpreadRadius);
	window->generateCircleTexture(&lightningTexture, lightningSpreadRadius);
}

/// \brief Calls SDL_DestoryTexture on all textures created at init
void Woud::destroyTextures() {
	SDL_DestroyTexture(treeTexture);
	SDL_DestroyTexture(fireTexture);
	SDL_DestroyTexture(bloomTexture);
	SDL_DestroyTexture(lightningTexture);
	// Status texture might not be defined
	if(statusTexture) {
		SDL_DestroyTexture(statusTexture);
	}
}

/// \param AText New text for the status texture
/// \brief If the status text has changed it will be rendered to the statusTexture.
void Woud::generateStatusTexture(const std::string& AText) {
	if(AText == statusText) {
		// The texture didn't change
		return;
	}
	statusText = AText;
	window->generateTextTexture(&statusTexture, AText, statusTextColor);
}

/// \brief Sets static properties of Tree to values defined in Woud
void Woud::initialiseTreeProperties() {
	Tree::maxSize            = treeMaxSize;
	Tree::bloomRadius        = treeBloomRadius;
	Tree::bloomRadiusSquared = treeBloomRadius * treeBloomRadius;
	Tree::bloomCyclesMax     = treeBloomCyclesMax;
	Tree::bloomLength        = treeBloomLength;
	Tree::maxAge             = treeMaxAge;
}

/// \brief Sets static properties of Lightning to values defined in Woud
void Woud::initialiseLightningProperties() {
	Lightning::maxAge = lightningMaxAge;
}

/// \brief Creates treeInitialSpawn trees and places them randomly in the window->windowRect
void Woud::initialiseTrees() {
	for(uint8_t i = 0; i < treeInitialSpawn; i++) {
		addTree(window->windowRect);
	}
}

/// \brief Deletes all the trees in the model
void Woud::destroyTrees() {
	for(std::list<Tree*>::iterator ATree = trees.begin(); ATree != trees.end(); ATree++) {
		delete *ATree;
	}
	trees.clear();
}

/// \param ATree Tree to process
/// \return False if the tree needs to be removed
/// \brief Processes a tree. Handles potential blooms and fire spreading.
/// Progresses the tree's life by one cycle.
bool Woud::processTree(Tree* ATree) {
	// Check if tree just changed type or if we're in a redraw
	if(ATree->age == 0 || doingRedraw) {
		// We'll draw it
		renderTree(ATree);
	}
	// Tree might be blooming
	if(ATree->state == Tree::STATE_BLOOMING) {
		addTree(ATree);
	}
	// Tree might be burning
	if(ATree->state == Tree::STATE_BURNING && ATree->age > fireSpreadIntensity) {
		spreadFire(ATree);
	}
	// Tree might be dying
	// use return to set it and pass needRedraw so tree can ask for a redraw if a tree was erased or stops blooming
	return !ATree->live(&needRedraw);
}

/// \brief Loops through all trees in the model and processes them.
///
/// Marks the current last tree in the list before the loop.
/// Recognises when it has passed this point and inserts newly added trees into the scene
void Woud::processTrees() {
	// Check if trees exist
	if(trees.size() == 0) {
		// If they don't they have all died, spawn some new ones.
		initialiseTrees();
	}
	// If sparseLoopStep ends up bigger than the number of trees we'll never
	// process them, so check here.
	if(sparseLoopStep > trees.size()) {
		sparseLoopStep = 0;
	}

	// Get the current last iterator
	Tree*                      ACurrentLast = trees.back();
	std::list<Tree*>::iterator ATree        = trees.begin();
	bool                       ALastPassed  = false;
	while(!ALastPassed) {
		ALastPassed = *ATree == ACurrentLast;
		// processTree returns true if the tree needs to be removed...
		if(sparseLoopStep == 0) {
			if( processTree(*ATree)) {
				// It died
				// Remove it from the scene
				scene->remove(*ATree);
				// Delete it
				delete *ATree;
				// Remove it from our list
				ATree = trees.erase(ATree);
			} else {
				// Staying alive
				ATree++;
			}
		} else {
			// We may still need to draw it...
			if((*ATree)->age == 0 || doingRedraw) {
				renderTree(*ATree);
			}
			ATree++;
		}
	}
	// We are now in the part of the list past the original end.
	// Trees in this part still have to be inserted into the scene
	while(ATree != trees.end()) {
		scene->insert(*ATree);
		ATree++;
	}
	// Modify sparseLoopStep to stagger.
	if(++sparseLoopStep >= sparseLoop) {
		sparseLoopStep = 0;
	}
}

/// \param ARect Rect to spawn the tree in
///
/// \brief Adds a tree to the model initialised to a random position inside the passed Rect
///
/// Inserts tree inside quadtree immediately
/// Renders the Tree.
void Woud::addTree(const Rect& ARect) {
	if(Population::populations[Population::TYPE_TOTAL] < popcap) {
		std::uniform_int_distribution<int> AWidthDistribution(ARect.left, ARect.right);
		std::uniform_int_distribution<int> AHeightDistribution(ARect.bottom, ARect.top);
		Vec                                APosition{AWidthDistribution(randomGenerator), AHeightDistribution(randomGenerator)};
		if(APosition.x < window->windowRect.left || APosition.x > window->windowRect.right || APosition.y < window->windowRect.bottom || APosition.y > window->windowRect.top) {
			// Trees can't spawn outside the window
			return;
		}
		try {
			Tree* ATree = new Tree(APosition);
			Population::registerChange(Population::TYPE_NEW, ATree->getPopulationType());
			trees.push_back(ATree);
			scene->insert(ATree);
			renderTree(ATree);
		} catch(...) {
			popcap = Population::populations[Population::TYPE_TOTAL];
			printf("Set popcap: %" PRIu64 "\n", popcap);
		}
	}
}

#ifdef USE_FASTADD
/// \param AParent Tree to spawn within the bloomradius of
///
/// \brief (USE_FASTADD) Spawns a tree within the bloomradius of another tree
///
/// Generates x and y and does a check to see whether it falls outside the bloomradius.
/// If so drops it and returns.
/// Adds tree to the internal list but does not insert it into the scene.
/// Renders the tree.
///
/// \attention This function will be used when USE_FASTADD is defined for the preprocessor
void Woud::addTree(const Tree* AParent) {
	if(Population::populations[Population::TYPE_TOTAL] < popcap) {
		std::uniform_int_distribution<int> AWidthDistribution(AParent->position.x - Tree::bloomRadius, AParent->position.x + Tree::bloomRadius);
		std::uniform_int_distribution<int> AHeightDistribution(AParent->position.y - Tree::bloomRadius, AParent->position.y + Tree::bloomRadius);
		int                                AX = AWidthDistribution(randomGenerator);
		int                                AY = AHeightDistribution(randomGenerator);
		// Check if a seed was generated outside the bloomradius and drop it.
		if(std::pow(AParent->position.x - AX, 2) + std::pow(AParent->position.y - AY, 2) > Tree::bloomRadiusSquared) {
			return;
		}
		Vec APosition{AX, AY};
		if(APosition.x < window->windowRect.left || APosition.x > window->windowRect.right || APosition.y < window->windowRect.bottom || APosition.y > window->windowRect.top) {
			// Trees can't spawn outside the window
			return;
		}
		// Test for intersect if so, drop it
		if(scene->collides(APosition)) {
			return;
		}

		try {
			Tree* ATree = new Tree(APosition);
			Population::registerChange(Population::TYPE_NEW, ATree->getPopulationType());
			trees.push_back(ATree);
			renderTree(ATree);
		} catch(...) {
			popcap = Population::populations[Population::TYPE_TOTAL];
			printf("Set popcap: %" PRIu64 "\n", popcap);
		}
	}
}

#else

/// \param AParent Tree to spawn within the bloomradius of
///
/// \brief (!USE_FASTADD) Spawns a tree within the bloomradius of another tree
///
/// Generate a random x and then calculates a maximum and minimum y for the spawned tree to be confined by a circle
/// defined by the parent tree's position and the bloomradius.
/// Adds tree to the internal list but does not insert it into the scene.
/// Renders the tree.
///
/// \attention This function will be used when USE_FASTADD is not defined for the preprocessor
void Woud::addTree(const Tree* AParent) {
	if(Population::populations[Population::TYPE_TOTAL] < popcap) {
		std::uniform_int_distribution<int> AWidthDistribution(AParent->position.x - Tree::bloomRadius, AParent->position.x + Tree::bloomRadius);
		int                                AX = AWidthDistribution(randomGenerator);
		// Calculate a maximum and minimum y so we spawn in a circle
		uint8_t                            AMaxOffset = std::sqrt(std::pow(Tree::bloomRadius, 2) - std::pow(AX - AParent->position.x, 2));
		std::uniform_int_distribution<int> AHeightDistribution(AParent->position.y - AMaxOffset, AParent->position.y + AMaxOffset);
		Vec                                APosition{AX, AHeightDistribution(randomGenerator)};
		// Trees can't spawn outside the window
		if(APosition.x < window->windowRect.left || APosition.x > window->windowRect.right || APosition.y < window->windowRect.bottom || APosition.y > window->windowRect.top) {
			return;
		}
		// Test for intersect if so, drop it
		if(scene->collides(APosition)) {
			return;
		}
		try {
			Tree* ATree = new Tree(APosition);
			Population::registerChange(Population::TYPE_NEW, ATree->getPopulationType());
			trees.push_back(ATree);
			renderTree(ATree);
		} catch(...) {
			popcap = Population::populations[Population::TYPE_TOTAL];
			printf("Set popcap: %" PRIu64 "\n", popcap);
		}
	}
}
#endif

/// \param ATree Tree to spread fire from
///
/// \brief Spreads fire from a burning tree
///
/// Check whether the tree has missed too many chances to burn as defined in fireSpreadMissMax to save collision checks
/// It then generates a rect around the tree and retrieves a list of all trees in that rect from the quadtree
/// It will loop through the trees and first check if they ar enot already burning
/// It will then check whether the tree is actually inside the burnradius.
/// If so it sets the tree on fire and draws it
void Woud::spreadFire(Tree* ATree) {
	// Don't spread fire after too many misses
	if(ATree->burnMiss == fireSpreadMissMax) {
		return;
	}
	// Generate a rect to spread in
	Rect ADangerZone = {
		ATree->position.x - fireSpreadRadius,
		ATree->position.x + fireSpreadRadius,
		ATree->position.y - fireSpreadRadius,
		ATree->position.y + fireSpreadRadius
	};

	// Get trees that might catch fire
	std::list<Tree*> ATreesInDangerZone;
	scene->retrieve(&ATreesInDangerZone, ADangerZone);
	// See if we need to increase burnmiss
	if(ATreesInDangerZone.size() == 0) {
		ATree->burnMiss++;
	}
	// Do a collision check on all of them
	for(std::list<Tree*>::iterator ATarget = ATreesInDangerZone.begin(); ATarget != ATreesInDangerZone.end(); ATarget++) {
		if((*ATarget)->state != Tree::STATE_BURNING) {
			// Collision check will check whether the tree is actually inside the burn radius
			if(ATree->collides((*ATarget)->position, fireSpreadRadius)) {
				// It's caught fire!
				Population::registerChange((*ATarget)->getPopulationType(), Population::TYPE_BURNING);
				(*ATarget)->state = Tree::STATE_BURNING;
				(*ATarget)->age   = 0;
				// We want to draw the fire
				renderTree(ATree);
			}
		}
	}
}

/// \param ALightning Lightning to spread fire from
///
/// \brief Spreads fire from an active lighning
///
/// Copy of spreadFire from a tree but for lightning.
/// If so it sets the tree on fire and draws it
///
/// \todo Generalise this function
void Woud::spreadFire(const Lightning* ALightning) {
	// Generate a rect to spread in
	Rect ADangerZone = {
		ALightning->position.x - lightningSpreadRadius,
		ALightning->position.x + lightningSpreadRadius,
		ALightning->position.y - lightningSpreadRadius,
		ALightning->position.y + lightningSpreadRadius
	};

	// Get trees that might catch fire
	std::list<Tree*> ATreesInDangerZone;
	scene->retrieve(&ATreesInDangerZone, ADangerZone);
	// Do a collision check on all of them
	for(std::list<Tree*>::iterator ATarget = ATreesInDangerZone.begin(); ATarget != ATreesInDangerZone.end(); ATarget++) {
		if((*ATarget)->state != Tree::STATE_BURNING) {
			if((*ATarget)->collides(ALightning->position, lightningSpreadRadius)) {
				// It's caught fire!
				Population::registerChange((*ATarget)->getPopulationType(), Population::TYPE_BURNING);
				(*ATarget)->state = Tree::STATE_BURNING;
				(*ATarget)->age   = 0;
				renderTree(*ATarget);
			}
		}
	}
}

/// \param ARect Rect to spawn lightning in
/// \brief Spawns a lightning in a random position inside the rect.
/// \todo Generalise this function (copy of addTree(const Rect& ARect))
void Woud::addLightning(const Rect& ARect) {
	std::uniform_int_distribution<int> AWidthDistribution(ARect.left, ARect.right);
	std::uniform_int_distribution<int> AHeightDistribution(ARect.bottom, ARect.top);
	Vec                                APosition{AWidthDistribution(randomGenerator), AHeightDistribution(randomGenerator)};
	if(APosition.x < window->windowRect.left || APosition.x > window->windowRect.right || APosition.y < window->windowRect.bottom || APosition.y > window->windowRect.top) {
		// Lighning can't spawn outside the window
		return;
	}
	Lightning ALightning = Lightning(APosition);
	lightnings.push_back(ALightning);
	renderLightning(&ALightning);
}

/// \brief Processes all lightnings in the model and potentially spwans new ones
///
/// Called every iteration
/// Adds a random lightning using addLightning() if the random number says so
/// Loops through all lightnings and lives them, spreading fire if they remain active
/// removes them otherwise
void Woud::processLightnings() {
	// Lightning!
	if(lightningDistribution(randomGenerator) == 0) {
		addLightning(window->windowRect);
	}
	// Check if there is lightning
	if(lightnings.size() == 0) {
		return;
	}
	// Loop through
	std::list<Lightning>::iterator ALightning = lightnings.begin();
	while(ALightning != lightnings.end()) {
		if(!ALightning->live()) {
			// It passed
			// Remove it from our list
			ALightning = lightnings.erase(ALightning);
			needRedraw = true;
		} else {
			// Draw it if we're doing a redraw
			if(doingRedraw) {
				renderLightning(&(*ALightning));
			}
			// It's causing a burn!
			spreadFire(&(*ALightning));
			// Staying alive
			ALightning++;
		}
	}
}

// Public

/// \param APath Base path of the application, used for loading assets
/// \param AWidth Width in pixels of the window to create
/// \param AHeight Height in pixels of the window to create
///
/// \brief Instantiates the application.
///
/// - Initialises static properties for objects
/// - Creates an SDLWindow to render everything in.
/// - Sets up the random number generators.
/// - Creates the quadtree to hold the population.
/// - Creates used textures
/// - Spawns a number of trees in random locations
Woud::Woud(const std::string& APath, const uint16_t& AWidth, const uint16_t& AHeight) {
	// Set static data on objects from our own settings
	initialiseTreeProperties();
	initialiseLightningProperties();

	// Use the path to find our font
	const std::string AFontPath = APath + "/assets/Inconsolata-Regular.ttf";
	// And launch our window
	window = new SDLWindow(AFontPath, statusTextSize, AWidth, AHeight);

	// Set up rng
	lightningDistribution = std::uniform_int_distribution<int>(0, lightningChance);
	std::random_device ARandomDevice;
	randomGenerator.seed(ARandomDevice());

	// Init
	// Set the scene
	scene = new QuadTree(0, window->windowRect);
	initialiseTextures();
	initialiseTrees();
}

/// \brief Destroys everything neatly.
///
/// Destroys:
/// - scene
/// - All trees in the model
/// - Textures
Woud::~Woud() {
	delete scene;
	scene = nullptr;
	destroyTrees();
	destroyTextures();
}

/// \brief Main application loop
///
/// Runs while gracefulShutdown has not been set
/// Every iteration it will:
/// - If it is not paused it will:
///   - Update the draw status
///	  - Processes the framerate
///   - Process lightnings
///   - Process the trees
///   - Rerender or draw if required
/// - Get events from the window and handle them
///
/// \todo Move event handling to separate function
void Woud::run() {
	// Wait for close
	bool ctrlPressed  = false;
	bool bPressed     = false;
	bool qPressed     = false;
	bool tPressed     = false;
	bool dPressed     = false;
	bool sPressed     = false;
	bool fPressed     = false;
	bool spacePressed = false;
	// The main loop
	while(!gracefulShutdown) {
		// Only do actual work if we're not paused
		if(!paused) {
			// Update framerate
			processFrameRate();
			// Do we need a redraw this round?
			if(needRedraw) {
				needRedraw  = false;
				doingRedraw = true;
				needDraw    = true;
				// Clear the window but don't present it on the screen
				window->clearScreen(backgroundColor, false);
			}
			// Process our simulation
			processLightnings();
			processTrees();
			// Render the last elements if we're doing a redraw
			if(doingRedraw) {
				if(drawQuadtree) {
					renderQuadTree();
				}
				if(drawStatus) {
					renderStatus();
				}
				doingRedraw = false;
			}
			// Present to screen if we have to
			// Use vsync to limit the framerate by drawing every iteration
			if(needDraw || (window->vsyncEnabled && capFrameRate)) {
				window->draw();
				needDraw = false;
			}
		}
		// Always check events, even in paused state
		// Poll or wait depending on pause state
		while(!gracefulShutdown &&  window->getEvent(event, paused)) {
			//User requests quit
			if(event.type == SDL_QUIT) {
				gracefulShutdown = true;
			} else if(event.type == SDL_KEYDOWN) {
				switch(event.key.keysym.sym) {
				case SDLK_LCTRL:
					ctrlPressed = true;
					break;
				case SDLK_q:
					qPressed = true;
					break;
				case SDLK_SPACE:
					if(!spacePressed) {
						paused = !paused;
					}
					spacePressed = true;
					break;
				case SDLK_t:
					if(!tPressed) {
						drawQuadtree = !drawQuadtree;
					}
					tPressed = true;
					break;
				case SDLK_d:
					if(!dPressed) {
						drawDangerZone = !drawDangerZone;
						needRedraw     = true;
					}
					dPressed = true;
					break;
				case SDLK_s:
					if(!sPressed) {
						drawStatus = !drawStatus;
						needRedraw = true;
					}
					sPressed = true;
					break;
				case SDLK_b:
					if(!bPressed) {
						drawBloom  = !drawBloom;
						needRedraw = true;
					}
					bPressed = true;
					break;
				case SDLK_f:
					if(!fPressed) {
						capFrameRate = !capFrameRate;
						std::string AInfo = capFrameRate ? "Cap on" : "Cap off";
						std::cout << AInfo << std::endl;
					}
					fPressed = true;
					break;
				}
			} else if(event.type == SDL_KEYUP) {
				switch(event.key.keysym.sym) {
				case SDLK_LCTRL:
					ctrlPressed = false;
					break;
				case SDLK_q:
					qPressed = false;
					break;
				case SDLK_SPACE:
					spacePressed = false;
					break;
				case SDLK_t:
					tPressed = false;
					break;
				case SDLK_d:
					dPressed = false;
					break;
				case SDLK_s:
					sPressed = false;
					break;
				case SDLK_b:
					bPressed = false;
					break;
				case SDLK_f:
					fPressed = false;
					break;
				}
			} else if(event.type == SDL_WINDOWEVENT) {
				switch(event.window.event) {
				case SDL_WINDOWEVENT_RESIZED:
					printf("Resized: Width: %u height: %u\n", event.window.data1, event.window.data2);
					break;
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					// Keep track of sizes
					window->windowRect.right = event.window.data1;
					window->windowRect.top   = event.window.data2;
					printf("Sizechanged: Width: %u height: %u\n", window->windowRect.right, window->windowRect.top);
					break;
				}
			}
			if(ctrlPressed) {
				if(qPressed) {
					gracefulShutdown = true;
				}
			}
		}
	}
}

/// \param ATree Tree to be drawn
///
/// \brief Draws a tree to the window
///
/// Color depends on tree state
/// Also draws bloom and/or fire if applicable
///
/// \note Sets needDraw to true
void Woud::renderTree(const Tree* ATree) {
	DCColor::RgbaColor AColor = treeColorGrowing;
	if(ATree->state == Tree::STATE_BURNING) {
		AColor = treeColorBurning;
		if(drawDangerZone) {
			// Draw dangerzone
			window->renderTexture(fireTexture, ATree->position, fireColor, fireSpreadRadius);
		}
	} else if(ATree->state == Tree::STATE_MATURE) {
		AColor = treeColorMature;
	} else if(ATree->state == Tree::STATE_BLOOMING) {
		AColor = treeColorBlooming;
		if(drawBloom) {
			// Draw bloom
			window->renderTexture(bloomTexture, ATree->position, bloomColor, treeBloomRadius);
		}
	} else if(ATree->state == Tree::STATE_DEAD) {
		AColor = treeColorDead;
	}
	window->renderTexture(treeTexture, ATree->position, AColor, treeMaxSize);
	needDraw = true;
}

/// \brief Loops through all trees in the model and draws them
void Woud::renderTrees() {
	for(std::list<Tree*>::iterator ATree = trees.begin(); ATree != trees.end(); ATree++) {
		renderTree(*ATree);
	}
}

/// \param ALightning Lightning to be drawn
/// \brief Draws a lightning to the window
/// \note Sets needDraw to true
void Woud::renderLightning(const Lightning* ALightning) {
	window->renderTexture(lightningTexture, ALightning->position, lightningColor, lightningSpreadRadius);
	needDraw = true;
}

/// \brief Loops through all lightnings in the model and draws them
void Woud::renderLightnings() {
	// Check if there is lightning
	if(lightnings.size() == 0) {
		return;
	}
	// Loop through
	for(std::list<Lightning>::iterator ALightning = lightnings.begin(); ALightning != lightnings.end(); ALightning++) {
		renderLightning(&(*ALightning));
	}
}

/// \brief Draws the quadtree onto the screen
/// \note Sets needDraw to true
void Woud::renderQuadTree() {
	std::list<Rect> AQuads;
	scene->retrieveQuads(&AQuads);
	for(std::list<Rect>::iterator AQuad = AQuads.begin(); AQuad != AQuads.end(); AQuad++) {
		window->renderRect(*AQuad, quadTreeColor);
	}
	needDraw = true;
}

/// \brief Creates a status texture from the current population. Draw to screen if necessary.
///
/// Draws it to the screen if the text has changed or if a redraw is needed
///
/// \note Sets needDraw to true
void Woud::renderStatus() {
	// Generate status text
	std::string AStatusText = " FPS: " + std::to_string(frameRate) +
							  " Pop: " + std::to_string(Population::populations[Population::TYPE_TOTAL]) +
							  " Grw: " + std::to_string(Population::populations[Population::TYPE_GROWING]) +
							  " Mat: " + std::to_string(Population::populations[Population::TYPE_MATURE]) +
							  " Blm: " + std::to_string(Population::populations[Population::TYPE_BLOOMING]) +
							  " Ded: " + std::to_string(Population::populations[Population::TYPE_DEAD]) +
							  " Brn: " + std::to_string(Population::populations[Population::TYPE_BURNING]) +
							  " Rem: " + std::to_string(Population::populations[Population::TYPE_REMOVED]);
	// Render text onto a texture
	generateStatusTexture(AStatusText);
	// Query texture size
	int ATextWidth, ATextHeight;
	SDL_QueryTexture(statusTexture, NULL, NULL, &ATextWidth, &ATextHeight);
	// Get the coordinates of the top of the colored background bar
	int ABarTop = window->windowRect.top - statusPadding - ATextHeight - statusPadding;
	// Make rects (These are SDL_Rects!)
	const SDL_Rect ASrcRect = {0, 0, ATextWidth, ATextHeight};
	const SDL_Rect ADstRect = {statusPadding, ABarTop + statusPadding, ATextWidth, ATextHeight};
	// Draw background bar
	window->renderFilledRect(Rect{window->windowRect.left, window->windowRect.right, window->windowRect.top, ABarTop}, statusBkgColor);
	// Copy our texture to the screen
	window->renderTextureCopy(statusTexture, ASrcRect, ADstRect);
	needDraw = true;
}

/// \brief Rerenders entire model onto screen
///
/// - Clears the screen to the background color
/// - Loops through trees and renders them
/// - Loops through lighnings and renders them
/// - Renders the quadTree if enabled
/// - Renders the status if enabled
/// - Draws to the screen
/// - Resets needDraw and needRedraw to false
void Woud::rerender() {
	window->clearScreen(backgroundColor, false);
	renderTrees();
	renderLightnings();
	if(drawQuadtree) {
		renderQuadTree();
	}
	if(drawStatus) {
		renderStatus();
	}
	window->draw();
	needRedraw  = false;
	doingRedraw = false;
	needDraw    = false;
}
