#include "sdlwindow.h"

// Private

/// \param AFontSize Point size (based on 72DPI) to load font as. This basically translates to pixel height.
/// \brief Initialises assets the window will need. Thus far it is just the font.
void SDLWindow::initialiseAssets(const uint8_t& AFontSize) {
	font = TTF_OpenFont(fontPath.c_str(), AFontSize);
	if(!font) {
		printf("Failed to load font! SDL_ttf Error: %s\n", TTF_GetError());
		exit(1);
	}
}

/// \param APreamble Text to output in front of version information
/// \param AVersion Version information retrieved from SDL
/// \brief Prints passed version information to stdout after prepending the APreamble
void SDLWindow::echoSdlVersion(const char* APreamble, const SDL_version* AVersion) {
	printf("%s %u.%u.%u\n", APreamble, AVersion->major, AVersion->minor, AVersion->patch);
}

/// \brief Retrieves compile- and run-time SDL Versions from SDL and passes them to echoSdlVersion()
void SDLWindow::echoSdlVersions() {
	SDL_version AVersion;

	// Prints the compile time version
	SDL_VERSION(&AVersion);
	echoSdlVersion("SDL compile-time version", &AVersion);

	// Prints the run-time version
	SDL_GetVersion(&AVersion);
	echoSdlVersion("SDL runtime version", &AVersion);
}

// Public

/// \param AFontPath Path to load font from used to render text
/// \param AFontSize Fontsize to load font with (see: SDLWindow::initialiseAssets())
/// \param AWidth Width of the screen
/// \param AHeight Height of the screen
///
/// \brief Sets up the SDL Window
///
/// - Outputs the version of SDL
/// - Initialises SDL Subsystems
///   - SDL
///   - TTF
///   Exits with a 1 if it fails
/// - Creates the window
///   Exits with a 1 if it fails
/// - Creates the renderer, will try several modes on failure of the previous
///   - Hardware acceleration and vsync
///   - Hardware acceleration without vsync
///   - Software acceleration
/// - Calls initialiseAssets()
///
/// Exits with a 1 if it fails
SDLWindow::SDLWindow(const std::string& AFontPath, const uint8_t& AFontSize, const uint16_t& AWidth, const uint16_t& AHeight) {
	fontPath = AFontPath;
	echoSdlVersions();

	// Keep track of sizes
	windowRect = Rect{0, AWidth, 0, AHeight};

	// Initialize SDL
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		exit(1);
	}

	//Initialize SDL_ttf
	if(TTF_Init() == -1) {
		printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
		exit(1);
	}

	// Create window
	window = SDL_CreateWindow("Woud", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, AWidth, AHeight, SDL_WINDOW_SHOWN);
	//window = SDL_CreateWindow("Woud", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, AWidth, AHeight, SDL_WINDOW_BORDERLESS);
	if(window == NULL) {
		printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		exit(1);
	}

	// Create renderer
	// First try with vsync
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(renderer == NULL) {
		printf("Hardware accelerated renderer with vsync could not be created! Trying without vsync. SDL_Error: %s\n", SDL_GetError());
		vsyncEnabled = false;
		// Try without vsync
		renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
		if(renderer == NULL) {
			printf("Hardware accelerated renderer could not be created! Trying software accelerated renderer. SDL_Error: %s\n", SDL_GetError());
			// Try software accelerated
			renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
			if(renderer == NULL) {
				printf("Software accelerated renderer could not be created! Giving up. SDL_Error: %s\n", SDL_GetError());
				exit(1);
			}
		}
	}
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

	// Init
	initialiseAssets(AFontSize);
}

/// \brief Cleans up all window objects and quits SDL
SDLWindow::~SDLWindow() {
	TTF_CloseFont(font);
	TTF_Quit();
	font = nullptr;
	SDL_DestroyRenderer(renderer);
	renderer = nullptr;
	SDL_DestroyWindow(window);
	window = nullptr;
	SDL_Quit();
}

/// \param AEvent Event to put any retrieved events in
/// \param AWait Whether the call should poll or wait and block until input is registered
/// \return Whether any events were in the queue to be retrieved
/// \brief Retrieves registered events from the SDL Window either polling or waiting and blocking depending on the AWait property
bool SDLWindow::getEvent(SDL_Event& AEvent, const bool AWait) {
	return AWait ? SDL_WaitEvent(&AEvent) != 0 : SDL_PollEvent(&AEvent) != 0;
}

/// \param ATexture Texture to render the circle to
/// \param ARadius Radius of the circle to be rendered
///
/// \brief Renders a fully white fully opague circle onto a texture
///
/// Loops through all the points in a diameter x diameter square
/// Fills the point if it fits within the circle
void SDLWindow::generateCircleTexture(SDL_Texture** ATexture, const uint8_t& ARadius) {
	uint16_t ADiameter = 2 * ARadius;
	*ATexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, ADiameter, ADiameter);
	SDL_SetTextureBlendMode(*ATexture, SDL_BLENDMODE_BLEND);
	// Use our renderer to render to the texture
	SDL_SetRenderTarget(renderer, *ATexture);
	// Set drawcolor
	SDL_SetRenderDrawColor(renderer, 0xff, 0xff, 0xff, 0xff);
	// Draw a circle
	for (int w = 0; w < ADiameter; w++) {
		for (int h = 0; h < ADiameter; h++) {
			int dx = ARadius - w;     // horizontal offset
			int dy = ARadius - h;     // vertical offset
			if ((dx*dx + dy*dy) <= (ARadius * ARadius)) {
				SDL_RenderDrawPoint(renderer, ARadius + dx, ARadius + dy);
			}
		}
	}
	// Return target to window
	SDL_SetRenderTarget(renderer, NULL);
}

/// \param ATexture Texture the text should be rendered to
/// \param AText Text to render
/// \param AColor Color of the text
///
/// \brief Uses the window's font to render passed text onto a passed texture in the given color
///
/// If the passed texture is defined is defined it will be destroyed.
/// A new texture is then generated from a textSurface created using the window font.
void SDLWindow::generateTextTexture(SDL_Texture** ATexture, const std::string& AText, const DCColor::RgbaColor& AColor) {
	//Render text surface
	SDL_Surface* ATextSurface = TTF_RenderText_Blended(font, AText.c_str(), SDL_Color{AColor.r, AColor.g, AColor.b, AColor.a});
	if(!ATextSurface) {
		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
	} else {
		//Create texture from surface pixels
		if(*ATexture) {
			SDL_DestroyTexture(*ATexture);
			*ATexture = nullptr;
		}
		*ATexture = SDL_CreateTextureFromSurface(renderer, ATextSurface);
		if(!*ATexture) {
			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
		}

		//Get rid of old surface
		SDL_FreeSurface(ATextSurface);
	}
}

/// \brief Presents the renderer's content onto the screen (Calls SDL_RenderPresent)
void SDLWindow::draw() {
	SDL_RenderPresent(renderer);
}

/// \param ADraw Whether to immediately render the new blank screen to the window
/// \param AColor Color to clear the screen to
/// \brief Clears the creen to the given color
void SDLWindow::clearScreen(const DCColor::RgbaColor& AColor, const bool ADraw) {
	SDL_SetRenderDrawColor(renderer, AColor.r, AColor.g, AColor.b, AColor.a);
	SDL_RenderClear(renderer);
	if(ADraw) {
		draw();
	}
}

/// \param ATexture Texture to be rendered onto the screen
/// \param ASrcRect SDL_Rect defining the region to be copied from the passed texture
/// \param ADstRect SDL_Rect defining the region it is to be copied to on the screen
/// \brief Copies a section of a texture onto a section of the screen
/// \note The passed Rects are SDL_Rect
void SDLWindow::renderTextureCopy(SDL_Texture* ATexture, const SDL_Rect& ASrcRect, const SDL_Rect& ADstRect) {
	SDL_RenderCopy(renderer, ATexture, &ASrcRect, &ADstRect);
}

/// \param ATexture Texture to be rendered
/// \param APosition Center of circle to render
/// \param AColor Color to render the circle in
/// \param ARadius Radius of the circle
/// \brief Renders a circle texture to the screen
void SDLWindow::renderTexture(SDL_Texture* ATexture, const Vec& APosition, const DCColor::RgbaColor& AColor, const uint8_t& ARadius) {
	// Get texture size
	int w, h;
	SDL_QueryTexture(ATexture, NULL, NULL, &w, &h);
	// Set drawcolor
	SDL_SetTextureColorMod(ATexture, AColor.r, AColor.g, AColor.b);
	SDL_SetTextureAlphaMod(ATexture, AColor.a);
	// Make rects
	SDL_Rect ASrcRect = {0, 0, w, h};
	SDL_Rect ADstRect = {APosition.x - ARadius, APosition.y - ARadius, w, h};
	// Copy our texture
	SDL_RenderCopy(renderer, ATexture, &ASrcRect, &ADstRect);
}

/// \param ARect Rect to render onto the screen
/// \param AColor Color to draw the outline with
/// \brief Renders the outline of a Rect to the screen
void SDLWindow::renderRect(const Rect& ARect, const DCColor::RgbaColor& AColor) {
	// Make SDL_rect
	SDL_Rect ASDLRect = {ARect.left, ARect.top,ARect.right - ARect.left + 1, ARect.bottom - ARect.top + 1};
	// Set drawcolor
	SDL_SetRenderDrawColor(renderer, AColor.r, AColor.g, AColor.b, AColor.a);
	SDL_RenderDrawRect(renderer, &ASDLRect);
}

/// \param ARect Rect to render onto the screen
/// \param AColor Color to fill the Rect with
/// \brief Renders a filled Rect to the screen
void SDLWindow::renderFilledRect(const Rect& ARect, const DCColor::RgbaColor& AColor) {
	// Make SDL_rect
	SDL_Rect ASDLRect = {ARect.left, ARect.top,ARect.right - ARect.left + 1, ARect.bottom - ARect.top + 1};
	// Set drawcolor
	SDL_SetRenderDrawColor(renderer, AColor.r, AColor.g, AColor.b, AColor.a);
	SDL_RenderFillRect(renderer, &ASDLRect);
}
