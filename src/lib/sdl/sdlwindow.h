#ifndef SDLWINDOW_H
#define SDLWINDOW_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include "geometry.h"
#include "../../utils/dccolor.h"

/// \brief An SDL window with interfaces to control rendering and retrieve user input
class SDLWindow {
	private:
		// Properties
		// Our window and surface
		SDL_Window*   window   = nullptr; ///< The main window object
		SDL_Renderer* renderer = nullptr; ///< The window's renderer
		// Font
		std::string fontPath;       ///< The path to load our ttf font from
		TTF_Font*   font = nullptr; ///< The font we will use when rendering text
		// Functions
		void initialiseAssets(const uint8_t& AFontSize);
		// Echo versions to stdout
		void echoSdlVersion(const char* APreamble, const SDL_version* AVersion);
		void echoSdlVersions();
	public:
		// Properties
		Rect windowRect;          ///< A rect representing our window boundaries
		bool vsyncEnabled = true; ///< Whether vsync is enabled

		// Functions
		// Create/destroy
		SDLWindow(const std::string& AFontPath, const uint8_t& AFontSize, const uint16_t& AWidth, const uint16_t& AHeight);
		~SDLWindow();
		// Application
		bool getEvent(SDL_Event& AEvent, const bool AWait = false);
		// Textures
		void generateCircleTexture(SDL_Texture** ATexture, const uint8_t &ARadius);
		void generateTextTexture(SDL_Texture** ATexture, const std::string &AText, const DCColor::RgbaColor &AColor);
		// Rendering
		void draw();
		void clearScreen(const DCColor::RgbaColor& AColor, const bool ADraw = true);
		void renderTextureCopy(SDL_Texture* ATexture, const SDL_Rect& ASrcRect, const SDL_Rect& ADstRect);
		void renderTexture(SDL_Texture* ATexture, const Vec& APosition, const DCColor::RgbaColor& AColor, const uint8_t& ARadius = 0);
		void renderRect(const Rect &ARect, const DCColor::RgbaColor& AColor);
		void renderFilledRect(const Rect &ARect, const DCColor::RgbaColor& AColor);
};

#endif // SDLWINDOW_H
