#include "dccolor.h"

/// \param hsv hsv struct to be converted into rgb
/// \brief Converts hsv color to rgb
/// \return The rgb equivalent of the passed hsv color
DCColor::RgbColor DCColor::hsvToRgb(HsvColor AHsv) {
	// Shamelessly copied from:
	// https://stackoverflow.com/questions/3018313/algorithm-to-convert-rgb-to-hsv-and-hsv-to-rgb-in-range-0-255-for-both
	RgbColor      ARgb;
	unsigned char ARegion, ARemainder, AP, AQ, AT;

	if (AHsv.s == 0) {
		ARgb.r = AHsv.v;
		ARgb.g = AHsv.v;
		ARgb.b = AHsv.v;
		return ARgb;
	}

	ARegion    = AHsv.h / 43;
	ARemainder = (AHsv.h - (ARegion * 43)) * 6;

	AP = (AHsv.v * (255 - AHsv.s)) >> 8;
	AQ = (AHsv.v * (255 - ((AHsv.s * ARemainder) >> 8))) >> 8;
	AT = (AHsv.v * (255 - ((AHsv.s * (255 - ARemainder)) >> 8))) >> 8;

	switch (ARegion) {
	case 0:
		ARgb.r = AHsv.v; ARgb.g = AT; ARgb.b = AP;
		break;
	case 1:
		ARgb.r = AQ; ARgb.g = AHsv.v; ARgb.b = AP;
		break;
	case 2:
		ARgb.r = AP; ARgb.g = AHsv.v; ARgb.b = AT;
		break;
	case 3:
		ARgb.r = AP; ARgb.g = AQ; ARgb.b = AHsv.v;
		break;
	case 4:
		ARgb.r = AT; ARgb.g = AP; ARgb.b = AHsv.v;
		break;
	default:
		ARgb.r = AHsv.v; ARgb.g = AP; ARgb.b = AQ;
		break;
	}

	return ARgb;
}

/// \param hsva hsva struct to be converted into rgba
/// \brief Converts hsva color to rgba
/// \return The rgba equivalent of the passed hsva color
DCColor::RgbaColor DCColor::hsvToRgb(DCColor::HsvaColor AHsva) {
	// Make an opacityless buffer
	HsvColor AHsvBuffer{AHsva.h, AHsva.s, AHsva.v};
	RgbColor ARgbBuffer = hsvToRgb(AHsvBuffer);
	return {ARgbBuffer.r, ARgbBuffer.g, ARgbBuffer.b, AHsva.a};
}
