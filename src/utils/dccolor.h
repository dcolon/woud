#ifndef DCCOLOR_H
#define DCCOLOR_H

///
/// \brief Class to play with colors nicely
class DCColor {
	private:
		DCColor();
	public:
		typedef struct RgbColor {
			unsigned char r;
			unsigned char g;
			unsigned char b;
		} RgbColor; ///< Struct containing date of a color in rgb

		typedef struct RgbaColor {
			unsigned char r;
			unsigned char g;
			unsigned char b;
			unsigned char a;
		} RgbaColor; ///< Struct containing date of a color in rgba (Added opacity)

		typedef struct HsvColor {
			unsigned char h;
			unsigned char s;
			unsigned char v;
		} HsvColor; ///< Struct containing date of a color in hsv

		typedef struct HsvaColor {
			unsigned char h;
			unsigned char s;
			unsigned char v;
			unsigned char a;
		} HsvaColor; ///< Struct containing date of a color in hsva (Added opacity)

		// Helpers
		RgbColor  hsvToRgb(HsvColor AHsv);
		RgbaColor hsvToRgb(HsvaColor AHsv);
};

#endif // DCCOLOR_H
