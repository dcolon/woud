#include <unistd.h>
#include <iostream>
#include "woud.h"

#define WINDOW_WIDTH  1000 ///< Width of screen upon start
#define WINDOW_HEIGHT 1000 ///< Height of screen upon start

/// \brief main
/// \return 0 if exited without errors
int main(int argc, char* args[]) {
	std::cout << "Starting woud" << std::endl;
	// Get the dir the application resides in
	std::string APath  = args[0];
	size_t      dirEnd = APath.find_last_of('/');
	APath = APath.substr(0, dirEnd);
	// Initialise and start the simulation
	Woud AWoud = Woud(APath, WINDOW_WIDTH, WINDOW_HEIGHT);
	AWoud.run();
	return 0;
}
