#ifndef LIGHTNING_H
#define LIGHTNING_H

#include <cstdint>
#include "../lib/sdl/geometry.h"

class Lightning {
	private:
		uint8_t age = 0; ///< Number of cycles the lightning has been active
	public:
		static uint8_t maxAge; ///< Number of cycles the lighning will be active, should be intialised by application
		Lightning(const Vec& APosition);
		Vec  position;
		bool live();
};

#endif // LIGHTNING_H
