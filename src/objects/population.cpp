#include "population.h"

/// Initialise populatrion count to 0
uint64_t Population::populations[Population::Type::TYPE_NEW] = {0};

/// \param AOldType Old population type of item
/// \param ANewType New population type of item
/// \brief Registers a change in population type for an item
void Population::registerChange(Population::Type AOldType, Population::Type ANewType) {
	// Decrease old pop count
	if(AOldType == TYPE_NEW) {
		populations[TYPE_TOTAL]++;
	} else {
		populations[AOldType]--;
	}
	if(ANewType == TYPE_REMOVED) {
		populations[TYPE_TOTAL]--;
	}
	populations[ANewType]++;
}
