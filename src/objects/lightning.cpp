#include "lightning.h"

// We declared this static property in the header. Now define it.
uint8_t Lightning::maxAge;

/// \param APosition Position to instantiate the lightning in
/// \brief Initialises the lighning to the passed position
Lightning::Lightning(const Vec& APosition) {
	position = APosition;
}

/// \brief Progresses the lightning through it's lifecycle
///
/// Adds one to the age and then returns whether it is greater than or equal too maxAge
///
/// \return Whether the lightning should be removed
bool Lightning::live() {
	age++;
	return age <= maxAge;
}
