#ifndef POPULATION_H
#define POPULATION_H

#include <cinttypes>

/// \brief Static class to keep track of population numbers
class Population {
	public:
		// Structs/enums/consts
		enum Type {
			TYPE_TOTAL,
			TYPE_GROWING,
			TYPE_MATURE,
			TYPE_BLOOMING,
			TYPE_DEAD,
			TYPE_BURNING,
			TYPE_REMOVED,
			TYPE_NEW
		}; ///< Enum containing the different types of population
		// Properties
		static uint64_t populations[Type::TYPE_NEW]; ///< Array containing the population count for every state
		// Functions
		static void registerChange(Type AOldType, Type ANewType);
};

#endif // POPULATION_H
