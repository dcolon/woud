#ifndef TREE_H
#define TREE_H

#include <cinttypes>
#include <cstdlib>
#include <cmath>
#include "../lib/sdl/geometry.h"
#include "population.h"

/// \brief Class representing a tree
class Tree {
	public:
		// Structs/enums/consts
		enum State {
			STATE_GROWING,
			STATE_MATURE,
			STATE_BLOOMING,
			STATE_DEAD,
			STATE_BURNING
		}; ///< Struct defining possible states of the tree

		// Properties
		/// @name Static properties shared amonst all trees
		/// @{
		/// \brief Should be initialised by the model
		static uint8_t  maxSize;            ///< The maximum size of a tree
		static uint8_t  bloomRadius;        ///< The radius in which a blooming tree may spawn children
		static uint16_t bloomRadiusSquared; ///< The radius in which a blooming tree may spawn children squared. Used for collisions.
		static uint8_t  bloomCyclesMax;     ///< How often a tree will bloom before dying
		static uint8_t  bloomLength;        ///< Duration in cycles of a bloom period
		static uint8_t  maxAge;             ///< Count of cycles before the tree eters a new state on live()
		/// @}

		Vec     position;                    ///< Location of the center of the tree
		Rect    bounds;                      ///< Hitbox for the tree
		State   state       = STATE_GROWING; ///< Current state of the tree
		uint8_t age         = 0;             ///< Count of cycles the tree has lived in the current state
		uint8_t size        = 0;             ///< The size of the tree
		uint8_t bloomCycles = 0;             ///< How many times the tree has bloomed
		uint8_t burnMiss    = 0;             ///< How often we did a collision check to spread fire and hit nothing

		// Functions
		Tree(const Vec& APosition);
		bool             live(bool* ANeedRedraw);
		bool             collides(const Tree* ATree);
		bool             collides(const Vec& AVec);
		bool             collides(const Vec& AVec, const uint16_t& ARadius);
		Population::Type getPopulationType();
};

#endif // TREE_H
