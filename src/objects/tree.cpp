#include "tree.h"

// We declared these static properties in the header. Now define them.
uint8_t  Tree::maxSize;
uint8_t  Tree::bloomRadius;
uint16_t Tree::bloomRadiusSquared;
uint8_t  Tree::bloomCyclesMax;
uint8_t  Tree::bloomLength;
uint8_t  Tree::maxAge;

/// \param APosition Position of the tree
/// \brief Instantiates a tree in the given position and creates it's boundingBox
Tree::Tree(const Vec& APosition) {
	position = APosition;
	bounds   = {
		position.x - maxSize,
		position.x + maxSize,
		position.y - maxSize,
		position.y + maxSize
	};
}

/// \param ANeedRedraw needRedraw can be passed for the tree to signal a redraw is needed (When it moves out of a blooming state or gets removed)
///
/// \brief Progresses the tree through it's lifecycle
///
/// Checks the state the tree is in and takes action accordingly.
/// Registers any state changes with the population.
///
/// Life cycle consists of moving through states following simple rules:
///
/// If the tree is growing it:
/// - Increases it's age and size by one every cycle
/// - If it reaches max age it:
///   - Progresses into mature
///
/// If the tree is mature it:
/// - Increases it's age by one every cycle
/// - If it reaches max age it:
///   - Progresses into dead when it has reacvhed it's maxBloomCycles
///   - Otherwise progresses into blooming
///
/// If the tree is blooming it:
/// - Increases it's age by one every cycle (Blooming is handled in Woud)
/// - If it reaches max age it:
///   - Progresses into mature
///
/// If the tree is dead it:
/// - Increases it's age and decreases it's size by one every cycle (Blooming is handled in Woud)
/// - If it reaches max age it:
///   - Gets removed
///
/// If the tree is burning it:
/// - Increases it's age and decreases it's size by ten every cycle (Burn spreading is handled in Woud)
/// - If it reaches zero size it:
///   - Gets removed
///
/// \return Returns false when the tree should be removed (It failed to live)
bool Tree::live(bool* ANeedRedraw) {
	// Check current state
	if(state == STATE_GROWING) {
		// Check state transition
		if(age == maxAge) {
			age = 0;
			Population::registerChange(getPopulationType(), Population::TYPE_MATURE);
			state = STATE_MATURE;
		} else {
			age++;
			size++;
		}
	} else if(state == STATE_MATURE) {
		if(age == maxAge) {
			age = 0;
			// We either bloom or die
			if(bloomCycles < bloomCyclesMax) {
				Population::registerChange(getPopulationType(), Population::TYPE_BLOOMING);
				state = STATE_BLOOMING;
			} else {
				Population::registerChange(getPopulationType(), Population::TYPE_DEAD);
				state = STATE_DEAD;
			}
		} else {
			age++;
		}
	} else if(state == STATE_BLOOMING) {
		// Return to a mature state after blooming
		if(age == bloomLength) {
			bloomCycles++;
			Population::registerChange(getPopulationType(), Population::TYPE_MATURE);
			*ANeedRedraw = true;
			state        = STATE_MATURE;
		}
		age++;
	} else if(state == STATE_DEAD) {
		if(age == maxAge) {
			// Get removed
			Population::registerChange(getPopulationType(), Population::TYPE_REMOVED);
			*ANeedRedraw = true;
			return false;
		}
		age++;
		size--;
	} else if(state == STATE_BURNING) {
		// Burn through our size
		if(size > 9) {
			// No need to bound check on age when burning since it'll subtract from size
			age  = age + 10;
			size = size - 10;
		} else {
			// Get removed
			Population::registerChange(getPopulationType(), Population::TYPE_REMOVED);
			*ANeedRedraw = true;
			return false;
		}
	}
	return true;
}

/// \param ATree Tree to check for collision with
///
/// \brief Checks if tree collides with another tree
///
/// Calculates a difference vector and compare it's squared length to the square of the trees' combined maxDrawSize (because yuck to sqrt)
///
/// \return Whether the trees collides
bool Tree::collides(const Tree* ATree) {
	return (pow(position.x - ATree->position.x, 2) + pow(position.y - ATree->position.y, 2)) < pow(maxSize + ATree->maxSize, 2);
}

/// \param AVec Point to check for collsion with
///
/// \brief Checks if tree collides with a point
///
/// Calculates a difference vector and compare it's squared length to the square of the tree's maxDrawSize (because yuck to sqrt)
///
/// \return Whether the trees collides
bool Tree::collides(const Vec& AVec) {
	return (pow(position.x - AVec.x, 2) + pow(position.y - AVec.y, 2)) < pow(maxSize, 2);
}

/// \param AVec Position of the circle to check for collision with
/// \param ARadius Radius of the circle to check for collision with
///
/// \brief Checks if tree collides with a circle
///
/// Calculates a difference vector and compare it's squared length to the square of the tree's maxDrawSize + ARadius (because yuck to sqrt)
///
/// \return Whether the trees collides
bool Tree::collides(const Vec& AVec, const uint16_t& ARadius) {
	return (pow(position.x - AVec.x, 2) + pow(position.y - AVec.y, 2)) < pow(maxSize + ARadius, 2);
}

/// \return The population type matching the tree's current state
/// \brief Translates the tree's state into a population type
Population::Type Tree::getPopulationType() {
	if(state == STATE_GROWING) {
		return Population::TYPE_GROWING;
	} else if(state == STATE_MATURE) {
		return Population::TYPE_MATURE;
	} else if(state == STATE_BLOOMING) {
		return Population::TYPE_BLOOMING;
	} else if(state == STATE_DEAD) {
		return Population::TYPE_DEAD;
	} else if(state == STATE_BURNING) {
		return Population::TYPE_BURNING;
	}
	throw;
}
