#ifndef WOUD_H
#define WOUD_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <random>
#include <list>
#include <ctime>
#include "lib/sdl/geometry.h"
#include "lib/sdl/sdlwindow.h"
#include "utils/dccolor.h"
#include "objects/tree.h"
#include "objects/lightning.h"
#include "objects/population.h"
#include "quadtree.h"

/// \brief Handles the simulation logic and contains the model
class Woud {
	private:
		/// @name Simulation settings:
		/// @{
		/// \brief Constant settings for the simulation
		// Trees
		const uint8_t treeInitialSpawn   = 20;        ///<How many trees to spawn during init.
		const uint8_t treeMaxSize        = 2;         ///< The maximum size of a tree
		const uint8_t treeBloomRadius    = 50;        ///< The radius in which a blooming tree may spawn children
		const uint8_t treeBloomCyclesMax = 1;         ///< How often a tree will bloom before dying
		const uint8_t treeBloomLength    = 3;         ///< Duration in cycles of a bloom period
		const uint8_t treeMaxAge         = UINT8_MAX; ///< Count of cycles before the tree eters a new state on live()
		// Lightning
		const uint16_t lightningChance       = 300;   ///< The chance lightning will strike is 1/n for every iteration
		const uint8_t  lightningSpreadRadius = 10;    ///< The radius over which fire will spread from lightning
		const uint8_t  lightningMaxAge       = 20;    ///< Number of cycles the lighning will be active
		// Fire
		const uint8_t fireSpreadIntensity = 30;       ///< The intensity a fire has to reach before spreading
		const uint8_t fireSpreadRadius    = 20;       ///< The radius over which fire will spread from a burning tree
		const uint8_t fireSpreadMissMax   = 2;        ///< How often a fire has to fail spreading before no collisions are checked
		// Optimisation
		const uint8_t sparseLoop = 8; ///< How many trees to actually process in each event loop = 1/n.
		/// @}

		/// @name Color settings:
		/// @{
		/// \brief Constant settings for drawing
		const DCColor::RgbaColor backgroundColor = {15, 63, 7, 255};     ///< Background color to render
		// Status
		const uint8_t            statusTextSize  = 14;                   ///< Size of the status bar text
		const uint8_t            statusPadding   = 5;                    ///< Padding of the status bar
		const DCColor::RgbaColor statusTextColor = {255, 255, 255, 255}; ///< Color of the status text
		const DCColor::RgbaColor statusBkgColor  = {0, 0, 0, 100};       ///< Color of the status background
		// Tree color
		const DCColor::RgbaColor treeColorGrowing  = {69, 232, 58, 255};  ///< Color of a growing Tree
		const DCColor::RgbaColor treeColorMature   = {51, 173, 43, 255};  ///< Color of a mature Tree
		const DCColor::RgbaColor treeColorBlooming = {193, 239, 55, 255}; ///< Color of a blooming Tree
		const DCColor::RgbaColor treeColorDead     = {173, 121, 8, 255};  ///< Color of a dead Tree
		const DCColor::RgbaColor treeColorBurning  = {214, 49, 8, 255};   ///< Color of a burning Tree
		// Specials
		const DCColor::RgbaColor fireColor      = {treeColorBurning.r, treeColorBurning.g, treeColorBurning.b, 40};    ///< Color to draw burn
		const DCColor::RgbaColor bloomColor     = {treeColorBlooming.r, treeColorBlooming.g, treeColorBlooming.b, 10}; ///< Color to draw bloom
		const DCColor::RgbaColor lightningColor = {66, 226, 244, 150};                                                 ///< Color to draw lightning
		const DCColor::RgbaColor quadTreeColor  = {255, 255, 255, 30};                                                 ///< Color to draw quadtree
		/// @}

		/// @name Window
		/// @{
		/// \brief The window the simulation is displayed in and related items
		///
		/// If a need for a redraw is detected during the main loop needRedraw is set.
		/// At the start of the next iteration the window will be cleared.
		/// needRedraw will be set to false and doingRedraw will be set. needDraw will be set as well.
		/// This will cause every object being iterated over to be rendered to the screen.
		/// This means there can be a one-frame lag in drawing objects.
		SDLWindow* window; ///< The SDLWindow displaying the game
		SDL_Event  event;  ///< Events read from the window like button events

		// Draw status
		/// This is the note for draw status
		/// Where is this shown?
		bool needDraw    = false;     ///< Set to true when something has been rendered and needs to be presented on the screen
		bool needRedraw  = false;     ///< Set to true when it is determined we'll need a redraw in the next iteration
		bool doingRedraw = false;     ///< needRedraw was set last iteration so every object looped over should be drawn during this iteration

		// Textures
		SDL_Texture* treeTexture      = nullptr; ///< Texture used when rendering trees
		SDL_Texture* bloomTexture     = nullptr; ///< Texture used when rendering the bloom of a tree
		SDL_Texture* fireTexture      = nullptr; ///< Texture used when rendering fire
		SDL_Texture* lightningTexture = nullptr; ///< Texture used when rendering lightning
		SDL_Texture* statusTexture    = nullptr; ///< Texture used when rendering the status.
		/// @}

		/// @name Toggles:
		/// @{
		/// \brief Bools that can be toggled through user interaction to control the application
		bool gracefulShutdown = false; ///< Whether a shutdown has been requested by the user
		bool paused           = false; ///< Whether execution has been paused
		bool capFrameRate     = false;   ///< Whether the framerate should be capped if vsync is enabled
		bool drawQuadtree     = true;  ///< Whether the quad tree should be drawn
		bool drawDangerZone   = true;  ///< Whether the radius fire will spread around a tree should be drawn
		bool drawStatus       = true;  ///< Whether the status bar should be shown
		bool drawBloom        = true;  ///< Whether the radius trees may spawn new trees during blooming should be drawn
		/// @}

		/// @name Framerate:
		/// @{
		/// \brief Properties used to keep track of framerate
		// Framerate
		std::time_t frameRateLastSampleTime = 0;     ///< The previous timestamp we wrote the framerate from the buffer
		uint16_t    frameRate               = 0;     ///< Last count of iterations run during a second
		uint16_t    frameRateBuffer         = 0;     ///< Count of iterations run since last sample
		/// @}

		/// @name Model:
		/// @{
		/// \brief Our population and related aspects.
		// Model
		uint64_t             popcap = UINT32_MAX;     ///< Limit of the population of trees. Set when failing to allocate memory.
		QuadTree*            scene;                   ///< QuadTree containing our woud
		std::list<Tree*>     trees;                   ///< Every tree in our population
		std::list<Lightning> lightnings;              ///< Currently active lightnings
		std::string          statusText = "";         ///< Text to render in the status bar
		uint8_t              sparseLoopStep = 0;      ///< Track beginning step of sparseLoop so we can stagger.
		/// @}

		/// @name Rng:
		/// @{
		/// \brief Random number generation
		// Rng
		std::mt19937                       randomGenerator;           ///< Standard mersenne_twister_engine seeded with rd()
		std::uniform_int_distribution<int> lightningDistribution;     ///< Random distribution to determine whether lightning will strike
		/// @}

		// Functions
		void processFrameRate();
		// Graphics
		void initialiseTextures();
		void destroyTextures();
		void generateStatusTexture(const std::string& AText);
		// Interaction with objects
		// Trees
		void initialiseTreeProperties();
		void initialiseLightningProperties();
		void initialiseTrees();
		void destroyTrees();

		bool processTree(Tree* ATree);
		void processTrees();

		void addTree(const Rect &ARect);
		void addTree(const Tree* AParent);
		// Spreading fire
		void spreadFire(Tree* ATree);
		void spreadFire(const Lightning* ALightning);
		// Lightning
		void addLightning(const Rect &ARect);
		void processLightnings();
	public:

		// Initialisation
		Woud(const std::string& APath, const uint16_t &AWidth, const uint16_t &AHeight);
		~Woud();
		// Simulation
		void run();
		// Rendering
		void renderTree(const Tree* ATree);
		void renderTrees();
		void renderLightning(const Lightning* ALightning);
		void renderLightnings();
		void renderQuadTree();
		void renderStatus();
		void rerender();
};

#endif // WOUD_H
