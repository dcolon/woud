#include "quadtree.h"

/// \brief Split the quadtree into four subsections
///
/// Checks if the max capacity has been passed before splitting.
/// Checks if we were already split.
///
/// \warning Do not call this during an iteration
void QuadTree::split() {
	// Check if we have exceeded our max capacity
	if(contents.size() > maxContent && level < maxLevel) {
		// Uh oh, we'll have to do something here
		if(!nodes[0]) {
			// We can split
			// Get the halfway point
			Vec AMidPoint = getMidPoint();
			// Generate four new nodes
			// Top right
			nodes[0] = new QuadTree(level + 1, Rect{AMidPoint.x, bounds.right, AMidPoint.y, bounds.top});
			// Top left
			nodes[1] = new QuadTree(level + 1, Rect{bounds.left, AMidPoint.x, AMidPoint.y, bounds.top});
			// Bottom left
			nodes[2] = new QuadTree(level + 1, Rect{bounds.left, AMidPoint.x, bounds.bottom, AMidPoint.y});
			// Bottom right
			nodes[3] = new QuadTree(level + 1, Rect{AMidPoint.x, bounds.right, bounds.bottom, AMidPoint.y});

			// Redistribute as many trees as we can to children
			std::list<Tree*>::iterator ATree = contents.begin();
			while(ATree != contents.end()) {
				// Does the tree fit into a subnode?
				int AIndex = getIndex((*ATree)->bounds);
				if(AIndex != -1) {
					// Move it to the subnode
					nodes[AIndex]->insert((*ATree));
					// Delete it from our list and advance the iterator
					ATree = contents.erase(ATree);
				} else {
					// The tree should stay
					ATree++;
				}
			}
		}
	}
}

/// \brief Collapses the quad's childNodes into itself
///
/// Checks if the min capacity has been passed before collapsing.
/// Checks if we were already collapsed.
void QuadTree::collapse() {
	if(nodes[0] && content < minContent) {
		for(uint8_t i = 0; i < 4; i++) {
			// Collapse childnode
			nodes[i]->collapse();
			// Retrieve it's content
			std::list<Tree*>* AChildContents = nodes[i]->getContents();
			for(std::list<Tree*>::iterator ATree = AChildContents->begin(); ATree != AChildContents->end(); ATree++) {
				contents.push_back(*ATree);
			}
			nodes[i]->destroyChildNodes();
			delete(nodes[i]);
			nodes[i] = nullptr;
		}
	}
}

/// \brief Recursively delete all childNodes
/// \warning If the childnodes contain pointers to objects you will lose track of them
void QuadTree::destroyChildNodes() {
	if(nodes[0]) {
		for(uint8_t i = 0; i < 4; i++) {
			nodes[i]->destroyChildNodes();
			delete(nodes[i]);
			nodes[i] = nullptr;
		}
	}
}

/// \param ARect Rect representing a boundingbox to get the index of
/// \brief Retrieves the index of the childNode for a new object to be inserted into
/// \return The index of the childNode if it fits, -1 if it fits in none of the childNodes
int QuadTree::getIndex(const Rect& ARect) {
	int AIndex = -1;
	// Get the halfway point
	Vec AMidPoint = getMidPoint();
	// Check whether the object fits inside the top/bottom half
	bool AFitsInTopHalf    = ARect.bottom > AMidPoint.y;
	bool AFitsInBottomHalf = ARect.top < AMidPoint.y;
	// Now check left/right
	if(ARect.right < AMidPoint.x) {
		// It fits in the left half
		if(AFitsInTopHalf) {
			AIndex = 1;
		} else if(AFitsInBottomHalf) {
			AIndex = 2;
		}
	} else if(ARect.left > AMidPoint.x) {
		// It fits in the right half
		if(AFitsInTopHalf) {
			AIndex = 0;
		} else if(AFitsInBottomHalf) {
			AIndex = 3;
		}
	}
	return AIndex;
}

/// \brief Calculates the midpoint of the quad
///
/// Checks if the midpoint is already known before calculating
///
/// \return The midpoint of the quad
Vec QuadTree::getMidPoint() {
	if(!midpointKnown) {
		midpoint.x    = bounds.left   + ((bounds.right - bounds.left)   / 2);
		midpoint.y    = bounds.bottom + ((bounds.top   - bounds.bottom) / 2);
		midpointKnown = true;
	}
	return midpoint;
}

/// \param ALevel Level of the quad
/// \param ABounds Bounds of the quad
/// \brief Initialises a new quad
QuadTree::QuadTree(uint8_t ALevel, Rect ABounds) {
	level  = ALevel;
	bounds = ABounds;
}

/// \brief Recursively deletes al childnodes
/// \note The contents will not be deleted
QuadTree::~QuadTree() {
	destroyChildNodes();
}

/// \brief Recursively destroy all content and call the delete operator on them. Also destroys all childnodes recursively.
/// \warning Pointers to the original instantiations of content of the tree will be dereferenced.
void QuadTree::destroyContents() {
	// First run through our childnodes
	if(nodes[0]) {
		for(uint8_t i = 0; i < 4; i++) {
			nodes[i]->destroyContents();
			delete(nodes[i]);
			nodes[i] = nullptr;
		}
	}
	// Then our contents
	for(std::list<Tree*>::iterator AIterator = contents.begin(); AIterator != contents.end(); AIterator++) {
		delete *AIterator;
	}
	// Then clear the list
	contents.clear();
}

/// \brief Direct interface to the private contents of the quad
/// \return This quad's contents
std::list<Tree*>*QuadTree::getContents() {
	return &contents;
}

/// \param ANewObject New object to be inserted
/// \brief Inserts a new object into the quadtree
///
/// - Increases the content by one
/// - If the quad has subnodes
///   - Get the index this object could fit into using getIndex()
///   - Insert it into the subnode if it fits
///   - Split if necessary
///   - Return
/// - If the quad has no subnodes
///   - Insert the new object into the quad's contents
///   - Split if necessary
void QuadTree::insert(Tree* ANewObject) {
	// Content counts children as well
	content++;
	// See if we even have subnodes
	if(nodes[0]) {
		// We have subnodes
		int AIndex = getIndex(ANewObject->bounds);
		if(AIndex != -1) {
			// It fits inside a subnode. Put it there
			nodes[AIndex]->insert(ANewObject);
			// We might want to split
			split();
			return;
		}
	}
	// We either have no nodes or the tree doesn't fit
	contents.push_back(ANewObject);

	// We might want to split
	split();
}

/// \param AObject Object to be removed
///
/// \brief Removes an object from the tree by it's coordinates
///
/// - Decreases content
/// - Looks up the object in the tree
/// - Removes it from the list if it is in us
/// - Collapses if necessary
void QuadTree::remove(Tree* AObject) {
	// Content counts children as well
	content--;
	// See if we even have subnodes
	if(nodes[0]) {
		// We have subnodes
		int AIndex = getIndex(AObject->bounds);
		if(AIndex != -1) {
			// It fits inside a subnode. Put it there
			nodes[AIndex]->remove(AObject);
			// We might want to collapse
			collapse();
			return;
		}
	}
	// Tree to be deleted should be in us
	contents.remove(AObject);

	// We might want to collapse
	collapse();
}

/// \param ACollisions Passed list that will be populated with collisions
/// \param ARect Rect to collide with
///
/// \brief QuadTree::retrieve
///
/// - If the quad has nodes
///   - Check if the passed Rect touches any of the childnodes
///   - If so call retrieve on them passing the passed list of collisions
/// - Adds all the quads contents to the passed list
void QuadTree::retrieve(std::list<Tree*>* ACollisions, const Rect& ARect) {
	// If it can collide with a child node recurse into it
	if(nodes[0]) {
		// Get the halfway point
		Vec AMidPoint = getMidPoint();
		// Check whether the object fits inside the top/bottom half
		bool ATouchesTopHalf    = ARect.top > AMidPoint.y;
		bool ATouchesBottomHalf = ARect.bottom < AMidPoint.y;
		// Now check left/right
		if(ARect.left < AMidPoint.x) {
			// It fits in the left half
			if(ATouchesTopHalf) {
				nodes[1]->retrieve(ACollisions, ARect);
			}
			if(ATouchesBottomHalf) {
				nodes[2]->retrieve(ACollisions, ARect);
			}
		}
		if(ARect.right > AMidPoint.x) {
			// It fits in the right half
			if(ATouchesTopHalf) {
				nodes[0]->retrieve(ACollisions, ARect);
			}
			if(ATouchesBottomHalf) {
				nodes[3]->retrieve(ACollisions, ARect);
			}
		}
	}
	// Copy our trees into collisions as wel
	for(std::list<Tree*>::iterator AIterator = contents.begin(); AIterator != contents.end(); AIterator++) {
		ACollisions->push_back(*AIterator);
	}
}

/// \param AObject Object to check for collisons with the tree's contents
/// \brief Simple check for a collision between passed object and anything in the tree
/// \return True if the object collides with something in the tree
bool QuadTree::collides(const Tree* AObject) {
	// First check our children
	for(std::list<Tree*>::iterator ATreeIterator = contents.begin(); ATreeIterator != contents.end(); ATreeIterator++) {
		if((*ATreeIterator)->collides(AObject)) {
			return true;
		}
	}
	// Get the index
	int AIndex = getIndex(AObject->bounds);
	if(AIndex != -1 && nodes[0]) {
		// If it can collide with a child node recurse into it
		if(nodes[AIndex]->collides(AObject)) {
			return true;
		}
	}
	// If we haven't returned check our leafnodes
	return false;
}

/// \param AVec Point to check for collisons with the tree's contents
/// \brief Simple check for a collision between passed object and anything in the tree
/// \return True if the point collides with something in the tree
bool QuadTree::collides(const Vec &AVec) {
	// First check our children
	for(std::list<Tree*>::iterator ATreeIterator = contents.begin(); ATreeIterator != contents.end(); ATreeIterator++) {
		if((*ATreeIterator)->collides(AVec)) {
			return true;
		}
	}
	// Get the index
	Rect APointRect = {AVec.x, AVec.x, AVec.y, AVec.y};
	int  AIndex     = getIndex(APointRect);
	if(AIndex != -1 && nodes[0]) {
		// If it can collide with a child node recurse into it
		if(nodes[AIndex]->collides(AVec)) {
			return true;
		}
	}
	return false;
}

/// \param ACallback
///
/// \brief Iterates through contents recursively and calls callback function
///
/// Iterates through all objects in self and children.
/// Calls the callback passing the object as an argument.
/// Will destroy the iterated object if the callback returns true.
/// Iterates through childNodes if the quad is split.
/// Returns the number of contents removed to track quad content through the recursion
///
/// \return Number of objects that were removed
/// \warning Pointers to destroyed content's original instantiation will be dereferenced
uint32_t QuadTree::iterate(std::function<bool (Tree*)> ACallback) {
	uint32_t AContentRemoved = 0;
	// First iterate through nodes
	if(nodes[0]) {
		for(uint8_t i = 0; i < 4; i++) {
			AContentRemoved += nodes[i]->iterate(ACallback);
		}
	}
	// Then loop through our contents
	std::list<Tree*>::iterator AIterator = contents.begin();
	while(AIterator != contents.end()) {
		if(ACallback(*AIterator)) {
			// Remove item from list
			delete(*AIterator);
			AIterator = contents.erase(AIterator);
			AContentRemoved++;
		} else {
			AIterator++;
		}
	}
	// Modify the quad content
	content -= AContentRemoved;
	return AContentRemoved;
}

/// \param AQuads List to append retrieved quads to
/// \brief Recursively retrieves the Rects of quads in the tree and appends them to the passed list
void QuadTree::retrieveQuads(std::list<Rect>* AQuads) {
	AQuads->push_back(bounds);
	if(nodes[0]) {
		nodes[0]->retrieveQuads(AQuads);
		nodes[1]->retrieveQuads(AQuads);
		nodes[2]->retrieveQuads(AQuads);
		nodes[3]->retrieveQuads(AQuads);
	}
}
