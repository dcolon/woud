#ifndef QUADTREE_H
#define QUADTREE_H

#include <cinttypes>
#include <list>
#include <cstdio>
#include <iostream>
#include <functional>
#include "lib/sdl/geometry.h"
#include "objects/tree.h"

/// \brief Quadtree facilitating lookup of objects by coordinates
class QuadTree final {
	private:
		// Constants
		uint16_t maxContent = 50; ///< Maximum content before quad splits
		uint16_t minContent = 40; ///< Minimum content before quad collapses
		uint8_t  maxLevel   = 10; ///< Maximum level to split to
		// Properties
		uint8_t          level   = 0;                                          ///< Level of this quad
		uint64_t         content = 0;                                          ///< Number of objects in this quad and all children
		std::list<Tree*> contents;                                             ///< Objects contained in this quad
		Rect             bounds;                                               ///< Rect representing bounds of this quad
		QuadTree*        nodes[4]      = {nullptr, nullptr, nullptr, nullptr}; ///< Childnodes of this quad. nullptr if not existant.
		bool             midpointKnown = false;                                ///< Check whether we have calculated our midpoint so we don't keep doing it
		Vec              midpoint      = {0, 0};                               ///< Midpoint of the quad
		// Functions
		void split();
		void collapse();
		void destroyChildNodes();
		int  getIndex(const Rect& ARect);
		Vec  getMidPoint();
	public:
		// Functions
		// Create/destroy
		QuadTree(uint8_t ALevel, Rect ABounds);
		~QuadTree();
		// QuadTree functions
		void              destroyContents();
		std::list<Tree*>* getContents();
		void              insert(Tree* ANewObject);
		void              remove(Tree* AObject);
		void              retrieve(std::list<Tree*>* ACollisions, const Rect& ARect);
		bool              collides(const Tree* AObject);
		bool              collides(const Vec& AVec);
		uint32_t          iterate(std::function<bool (Tree*)> ACallback);
		void              retrieveQuads(std::list<Rect>* AQuads);
};

#endif // QUADTREE_H
